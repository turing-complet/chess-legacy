[![Test Status](https://travis-ci.org/turing-complet/chess.svg?branch=master)](https://travis-ci.org/turing-complet/chess)

# Chess website
Very sophisticated website for playing chess :fish_cake:

**Setup**:
Requires docker and docker-compose

```
git clone https://github.com/turing-complet/chess.git
cd chess
docker-compose build
docker-compose up web
```

Initial database setup:

```
chmod +x db/initdb.sh
./initdb.sh
```

Then the site will be running on `localhost`
