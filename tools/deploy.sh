#!/bin/bash

#echo "Creating new build folder"
#mkdir -p builds/chess$1
#cd builds/chess$1

cd builds
echo "Downloading source files"
git pull

echo "Creating virtualenv"
source ../env/bin/activate 

echo "Installing packages"
pip install -r requirements.txt

echo "Upgrading database"
python tools/db_update.py --function --table

echo "Restarting server"
export FLASK_APP=chess_app/chess.py
flask run &
