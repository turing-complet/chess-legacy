#!/bin/bash

cd builds
echo "Downloading source files"
git pull

echo "Building docker"
./build.sh

echo "Starting container"
./run.sh
