from flask_script import Manager
from chess_app import a

manager = Manager(app)


# @staticmethod
# def insert_roles():
#     roles = {
#         'User': (Permission.GENERAL, 'main', True),
#         'Administrator': (
#             Permission.ADMINISTER,
#             'admin',
#             False  # grants all permissions
#         )
#     }
#     for r in roles:
#         role = Role.query.filter_by(name=r).first()
#         if role is None:
#             role = Role(name=r)
#         role.permissions = roles[r][0]
#         role.index = roles[r][1]
#         role.default = roles[r][2]
#         db_session.add(role)
#     db_session.commit()

# def setup_general():
#     """Runs the set-up needed for both local development and production.
#        Also sets up first admin user."""
#     Role.insert_roles()
#     admin_query = Role.query.filter_by(name='Administrator')
#     if admin_query.first() is not None:
#         if User.query.filter_by(email=Config.ADMIN_EMAIL).first() is None:
#             user = User(
#                 username='superadmin',
#                 first_name='Admin',
#                 last_name='Account',
#                 password=Config.ADMIN_PASSWORD,
#                 confirmed=True,
#                 email=Config.ADMIN_EMAIL)
#             db_session.add(user)
#             db_session.commit()
#             print('Added administrator {}'.format(user.full_name()))




if __name__ == '__main__':
    manager.run()