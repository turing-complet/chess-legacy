-- Table: public.games

-- DROP TABLE public.games;

CREATE TABLE public.games
(
    id uuid NOT NULL,
    start timestamp without time zone,
    finish timestamp without time zone,
    white integer,
    black integer,
    turn integer,
    CONSTRAINT games_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.games
    OWNER to postgres;