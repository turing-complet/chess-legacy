-- Table: public.game_history

-- DROP TABLE public.game_history;

CREATE TABLE public.game_history
(
    wp character(2)[] COLLATE pg_catalog."default" NOT NULL,
    wn character(2)[] COLLATE pg_catalog."default" NOT NULL,
    wb character(2)[] COLLATE pg_catalog."default" NOT NULL,
    wr character(2)[] COLLATE pg_catalog."default" NOT NULL,
    wq character(2)[] COLLATE pg_catalog."default" NOT NULL,
    wk character(2)[] COLLATE pg_catalog."default" NOT NULL,
    bp character(2)[] COLLATE pg_catalog."default" NOT NULL,
    bn character(2)[] COLLATE pg_catalog."default" NOT NULL,
    bb character(2)[] COLLATE pg_catalog."default" NOT NULL,
    br character(2)[] COLLATE pg_catalog."default" NOT NULL,
    bq character(2)[] COLLATE pg_catalog."default" NOT NULL,
    bk character(2)[] COLLATE pg_catalog."default" NOT NULL,
    game_id uuid NOT NULL,
    update_time timestamp
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.game_history
    OWNER to postgres;
