
CREATE SEQUENCE moves_id_seq;
CREATE TABLE public.moves
(
    id integer NOT NULL DEFAULT nextval('moves_id_seq'::regclass),
    game_id integer,
    move character varying(8) COLLATE pg_catalog."default" NOT NULL,
    userid integer NOT NULL,
    date timestamp without time zone,
    CONSTRAINT moves_pkey PRIMARY KEY (id),
    CONSTRAINT moves_userid_fkey FOREIGN KEY (userid)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.moves
    OWNER to postgres;