
CREATE SEQUENCE roles_id_seq;
CREATE TABLE public.roles
(
    id integer NOT NULL DEFAULT nextval('roles_id_seq'::regclass),
    name character varying(64) COLLATE pg_catalog."default",
    index character varying(64) COLLATE pg_catalog."default",
    is_default boolean,
    permissions integer,
    CONSTRAINT roles_pkey PRIMARY KEY (id),
    CONSTRAINT roles_name_key UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.roles
    OWNER to postgres;

