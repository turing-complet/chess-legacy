
CREATE SEQUENCE users_id_seq;
CREATE TABLE public.users
(
    id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    confirmed boolean,
    username character varying(64) COLLATE pg_catalog."default",
    first character varying(64) COLLATE pg_catalog."default",
    last character varying(64) COLLATE pg_catalog."default",
    email character varying(64) COLLATE pg_catalog."default",
    password_hash character varying(256) COLLATE pg_catalog."default",
    roleid integer,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;