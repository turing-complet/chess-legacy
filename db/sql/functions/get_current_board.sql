CREATE OR REPLACE FUNCTION get_current_board (_game_id UUID)
 RETURNS TABLE (
     wp character(2)[],
     wn character(2)[],
     wb character(2)[],
     wr character(2)[],
     wq character(2)[],
     wk character(2)[],
     bp character(2)[],
     bn character(2)[],
     bb character(2)[],
     br character(2)[],
     bq character(2)[],
     bk character(2)[],
     game_id uuid,
     update_time timestamp
)
AS $$
BEGIN
 RETURN QUERY
 SELECT *
	FROM game_history g
    WHERE g.game_id = _game_id
    ORDER BY g.update_time DESC
    LIMIT 1;
END; $$

LANGUAGE 'plpgsql';

