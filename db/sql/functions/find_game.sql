CREATE OR REPLACE FUNCTION find_game (game_id UUID)
 RETURNS TABLE (
     id UUID,
     start TIMESTAMP,
     finish TIMESTAMP,
     white INTEGER,
     black INTEGER,
     turn INTEGER
)
AS $$
BEGIN
 RETURN QUERY SELECT
 *
 FROM
 games g
 WHERE
 g.id = game_id;
END; $$ 
 
LANGUAGE 'plpgsql';

