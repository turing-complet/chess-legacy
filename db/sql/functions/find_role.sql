CREATE OR REPLACE FUNCTION find_role (role_name VARCHAR(64))
 RETURNS TABLE (
     id INTEGER,
     name VARCHAR(64),
     index VARCHAR(64),
     is_default BOOLEAN,
     permission INTEGER
)
AS $$
BEGIN
 RETURN QUERY SELECT
 r.id, r.name, r.index, r.default as is_default, permission
 FROM
 roles r
 WHERE
 r.name = role_name;
END; $$

LANGUAGE 'plpgsql';

