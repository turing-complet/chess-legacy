CREATE OR REPLACE FUNCTION get_user (user_name VARCHAR(64))
 RETURNS TABLE (
     userid INTEGER,
     confirmed BOOLEAN,
     username VARCHAR(64),
     first VARCHAR(64),
     last VARCHAR(64),
     email VARCHAR(64),
     password_hash VARCHAR(256),
     roleid INTEGER
)
AS $$
BEGIN
 RETURN QUERY SELECT
 u.id, u.confirmed, u.username, u.first, u.last, u.email, u.password_hash, u.roleid
 FROM
 users u
 WHERE
 u.username = user_name;
END; $$ 
 
LANGUAGE 'plpgsql';
