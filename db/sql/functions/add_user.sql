CREATE OR REPLACE FUNCTION add_user (
  confirmed BOOLEAN,
  username VARCHAR(64),
  first VARCHAR(64),
  last VARCHAR(64),
  email VARCHAR(64),
  password_hash VARCHAR(256),
  roleid INTEGER
)
 RETURNS void
AS $$
BEGIN
	INSERT INTO users (confirmed, username, first, last, email, password_hash, roleid)
    VALUES (confirmed, username, first, last, email, password_hash, roleid);
END; $$

LANGUAGE 'plpgsql';

