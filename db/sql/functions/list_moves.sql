CREATE OR REPLACE FUNCTION list_moves (gameid INTEGER)
 RETURNS TABLE (
     id INTEGER,
     game_id INTEGER,
     move VARCHAR(8),
     userid INTEGER,
     date TIMESTAMP
)
AS $$
BEGIN
 RETURN QUERY SELECT
 *
 FROM
 moves m
 WHERE
 m.game_id = gameid;
END; $$

LANGUAGE 'plpgsql';

