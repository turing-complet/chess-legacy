CREATE OR REPLACE FUNCTION add_move (
    game_id INTEGER,
    move_str VARCHAR(8),
    user_id INTEGER,
    move_date TIMESTAMP
)
 RETURNS void
AS $$
BEGIN
	INSERT INTO moves (game_id, move, userid, date)
    VALUES (game_id, move_str, user_id, move_date);
END; $$

LANGUAGE 'plpgsql';