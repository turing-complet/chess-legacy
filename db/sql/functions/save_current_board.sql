-- FUNCTION: public.save_current_board(uuid, timestamp without time zone, character[], character[], character[], character[], character[], character[], character[], character[], character[], character[], character[], character[])

-- DROP FUNCTION public.save_current_board(uuid, timestamp without time zone, character[], character[], character[], character[], character[], character[], character[], character[], character[], character[], character[], character[]);

CREATE OR REPLACE FUNCTION public.save_current_board(
	id uuid,
	update_time timestamp without time zone,
	wp character[],
	wn character[],
	wb character[],
	wr character[],
	wq character[],
	wk character[],
	bp character[],
	bn character[],
	bb character[],
	br character[],
	bq character[],
	bk character[])
RETURNS void
    LANGUAGE 'plpgsql'
AS $$

BEGIN
  INSERT INTO game_history (wP, wN, wB, wR, wQ, wK, bP, bN, bB, bR, bQ, bK, game_id, update_time)
  VALUES (wp, wn, wb, wr, wq, wk, bp, bn, bb, br, bq, bk, id, update_time);
END;

$$;
