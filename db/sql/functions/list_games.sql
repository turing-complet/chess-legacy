CREATE OR REPLACE FUNCTION list_games (userid INTEGER)
 RETURNS TABLE (
     id UUID,
     start TIMESTAMP,
     finish TIMESTAMP,
     white INTEGER,
     black INTEGER,
     turn INTEGER,
     username VARCHAR(64)
)
AS $$
BEGIN
 RETURN QUERY
  SELECT g.*, u.username
  FROM games g
  JOIN users u on u.id = g.white AND userid = g.black
  UNION
  SELECT g.*, u.username
  FROM games g
  JOIN users u on u.id = g.black AND userid = g.white
 WHERE
 g.white = userid OR g.black = userid;
END; $$

LANGUAGE 'plpgsql';