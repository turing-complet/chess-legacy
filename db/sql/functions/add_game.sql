CREATE OR REPLACE FUNCTION add_game (
    id UUID,
	  start TIMESTAMP,
    white INTEGER,
    black INTEGER,
    turn INTEGER
)
 RETURNS void
AS $$
BEGIN
	INSERT INTO games (id, start, finish, white, black, turn)
    VALUES (id, start, null, white, black, turn);
END; $$
 
LANGUAGE 'plpgsql';

