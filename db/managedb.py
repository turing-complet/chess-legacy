
import psycopg2
import os
import click
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from functools import wraps

import sys
sys.path.insert(1, os.path.join(sys.path[0], '..'))

from chess_app.config import Config
from chess_app.models import User, Role


DATABASE_URI = 'postgres://postgres:postgres@localhost:5432/'
DATABASE = 'chessdb'
CONN = DATABASE_URI + DATABASE


def _load_secrets(fileName):
    secrets = {}
    if not os.path.exists(fileName):
        raise IOError('Configuration not found')
    for line in open(fileName):
        var = line.strip().split('=')
        if len(var) == 2:
            secrets[var[0]] = var[1]
    return secrets


def dbcommand(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        with psycopg2.connect(CONN) as conn:
            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            with conn.cursor() as cursor:
                return f(cursor, *args, **kwargs)
    return decorated


def _create_roles(cursor):
    roles = {
        Role.default(), Role.admin()
    }
    for role in roles:
        cursor.execute('SELECT * FROM roles LIMIT 1')
        if len(cursor.fetchall()) == 0:
            sql = "INSERT INTO roles (name, permissions, index, is_default) VALUES (%s, %s, %s, %s);"
            data = (role.name, role.permissions, role.index, role.is_default)
            cursor.execute(sql, data)


def _create_admin_user(cursor):
    user = User.create(
            username='superadmin',
            first='Admin',
            last='Account',
            email=Config.ADMIN_EMAIL,
            password=Config.ADMIN_PASSWORD,
            roleid=1,
            )
    # add existence check
    sql = "INSERT INTO users (username, first, last, email, password_hash, confirmed, roleid) VALUES (%s, %s, %s, %s, %s, %s, %s);"
    data = (user.username, user.first, user.last, user.email, user.password_hash, user.confirmed, user.roleid)
    cursor.execute(sql, data)


def run_schema_updates(cursor, local_dir, base_dir='./sql'):
    dir = os.path.join(base_dir, local_dir)
    for file in os.listdir(dir):
        fq = os.path.abspath(os.path.join(dir, file))
        with open(fq, "r") as f:
            cmd = f.read()
            click.echo(cmd)
            cursor.execute(cmd)


def create_db():
    secrets = _load_secrets('secrets.env')
    with psycopg2.connect(DATABASE_URI) as conn:
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        with conn.cursor() as cur:
            cur.execute('SELECT 1 FROM pg_database WHERE datname = \'{}\''.format(DATABASE))
            exists = len(cur.fetchall()) > 0
            if not exists:
                cur.execute('CREATE DATABASE {}'.format(DATABASE))
                cur.execute('CREATE USER {} WITH PASSWORD \'{}\''
                            .format(secrets['username'], secrets['password']))
            else:
                click.echo("Database already exists")


def post_update(cursor):
    _create_roles(cursor)
    _create_admin_user(cursor)


def clean(cursor):
    click.echo("Cleaning the db")
    cursor.execute('DROP DATABASE {}'.format(DATABASE))


@click.command()
@click.option('--table', is_flag=True, help="update tables")
@click.option('--function', is_flag=True, help="update functions")
@click.option('--post', is_flag=True)
@click.option('--reset', is_flag=True)
@click.option('--init', is_flag=True)
@dbcommand
def db_update(cur, table, function, post, reset, init):
    if init:
        click.echo("Creating database")
        create_db()
    if table:
        click.echo("Updating tables")
        run_schema_updates(cur, 'tables')
    if function:
        click.echo("Updating functions")
        run_schema_updates(cur, 'functions')
    if post:
        click.echo("Creating default data")
        post_update(cur)
    if reset:
        click.echo("Removing db")
        clean(cur)


if __name__ == '__main__':
    db_update()
