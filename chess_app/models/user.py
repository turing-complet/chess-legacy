from flask import current_app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from werkzeug.security import check_password_hash, generate_password_hash


class Permission:
    GENERAL = 0x01
    ADMINISTER = 0xff


class Role:

    def __init__(self):
        self.id = None
        self.name = None
        self.index = None
        self.is_default = None
        self.permissions = None

    @staticmethod
    def create(name, index, is_default, permissions):
        r = Role()
        r.name = name
        r.index = index
        r.is_default = is_default
        r.permissions = permissions
        return r

    @staticmethod
    def default():
        return Role.create('User', 'main', True, Permission.GENERAL)

    @staticmethod
    def admin():
        return Role.create('Administrator', 'admin', False, Permission.ADMINISTER)

    def __repr__(self):
        return '<Role \'%s\'>' % self.name


class User:

    def __init__(self):
        self.userid = None
        self.username = None
        self.first = None
        self.last = None
        self.email = None
        self.password_hash = None
        self.confirmed = None
        self.roleid = None

    @staticmethod
    def create(username, first, last, email, password, roleid):
        user = User()
        user.username = username
        user.first = first
        user.last = last
        user.email = email
        user.password_hash = generate_password_hash(password)
        user.confirmed = False
        user.roleid = roleid
        return user

    def full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def can(self, permissions):
        return self.role is not None and \
            (self.role.permissions & permissions) == permissions

    def is_admin(self):
        return self.can(Permission.ADMINISTER)

    @property
    def password(self):
        raise AttributeError('`password` is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=604800):
        """Generate a confirmation token to email a new user."""

        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def generate_email_change_token(self, new_email, expiration=3600):
        """Generate an email change token to email an existing user."""
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def generate_password_reset_token(self, expiration=3600):
        """
        Generate a password reset change token to email to an existing user.
        """
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def confirm_account(self, token):
        """Verify that the provided token is for this user's id."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        return True

    def change_email(self, token):
        """Verify the new email for this user."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        return True

    def reset_password(self, token, new_password):
        """Verify the new password for this user."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        # db_session.add(self)
        # db_session.commit()
        return True

