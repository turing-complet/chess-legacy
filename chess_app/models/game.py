from datetime import datetime
import uuid
import logging

class Game:

    def __init__(self):
        self.id = uuid.uuid4()
        self.start = None
        self.end = None
        self.white = None
        self.black = None
        self.turn = None
        self.board = None
        self.opponent = None
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def new(white, black):
        game = Game()
        game.start = datetime.now()
        game.end = None
        game.white = white
        game.black = black
        game.turn = white
        return game

    def color(self, userid):
        if self.white == userid:
            return 'white'
        if self.black == userid:
            return 'black'
        raise ValueError('User is not part of this game')

    @staticmethod
    def build_from_invite(user1, user2, user2_color):
        if user2_color == 'black':
            return Game.new(user1, user2)
        else:
            return Game.new(user2, user1)





