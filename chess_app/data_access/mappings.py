from sqlalchemy import ForeignKey, Integer, String, Boolean, Text, DateTime
from sqlalchemy.orm import relationship
from chess_app.models import *

def init(db):
    game_mapping = db.Table('games',
        db.Column('id', Integer, primary_key=True),
        db.Column('start', DateTime),
        db.Column('end', DateTime),
        db.Column('white', Integer),
        db.Column('black', Integer),
    )

    move_mapping = db.Table('moves',
        db.Column('id', Integer, primary_key=True),
        db.Column('game_id', Integer),
        db.Column('move', String(8), nullable=False),
        db.Column('date', DateTime, nullable=False),
        db.Column('userid', Integer, ForeignKey("users.id"), nullable=False),
    )

    user_mapping = db.Table('users',
        db.Column('id', Integer, primary_key=True),
        db.Column('confirmed', Boolean),
        db.Column('username', String(64)),
        db.Column('first', String(64)),
        db.Column('last', String(64)),
        db.Column('email', String(64)),
        db.Column('password_hash', String(256)),
        db.Column('roleid', ForeignKey('roles.id')),
    )

    role_mapping = db.Table('roles',
        db.Column('id', primary_key=True),
        db.Column('name', String(64), unique=True),
        db.Column('index', String(64)),
        db.Column('default', Boolean),
        db.Column('permissions', Integer),
    )

    db.mapper(User, user_mapping, properties={
        'role': relationship(Role, backref="users")
    })
    db.mapper(Game, game_mapping)
    db.mapper(Move, move_mapping)
    db.mapper(Role, role_mapping)
