from psycopg2.extras import LoggingConnection, DictCursor
import psycopg2
import logging


class DatabaseException(Exception):
    pass


class RepositoryBase(object):
    def __init__(self, dsn):
        self.dsn = dsn
        self.logger = logging.getLogger(__name__)

    def run_sql(self, proc, **params):
        return self.run_sql_with_cursor(proc, None, **params)

    def run_sql_dict(self, proc, **params):
        return self.run_sql_with_cursor(proc, DictCursor, **params)

    def run_sql_with_cursor(self, proc, cursor_factory, **params):
        conn = None
        try:
            psycopg2.extras.register_uuid()
            conn = psycopg2.connect(self.dsn, connection_factory=LoggingConnection)
            conn.initialize(self.logger)
            cur = conn.cursor(cursor_factory=cursor_factory)
            cur.callproc(proc, params)
            rows = cur.fetchall()
            return rows

        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error('Database exception: %s', error)
            raise DatabaseException(error)

        finally:
            if conn is not None:
                # cur.close()
                conn.commit()
                conn.close()  # TODO: make this better