
from chess_app.data_access.repositorybase import RepositoryBase
from chess_app.models import User, Role
import logging

class UserRepository(RepositoryBase):

    def __init__(self, dsn):
        super(UserRepository, self).__init__(dsn)
        self.logger = logging.getLogger(__name__)

    def get_by_username(self, username):
        result = super().run_sql('get_user', user_name=username)
        if len(result) == 0:
            return None
        return self._to_user(result[0])

    def get_default_role(self):
        rows = super().run_sql('find_role', role_name='User')
        return self._to_role(rows[0])

    def add_user(self, user):
        super().run_sql('add_user',
                     confirmed=user.confirmed,
                     username=user.username,
                     first=user.first,
                     last=user.last,
                     email=user.email,
                     password_hash=user.password_hash,
                     roleid=user.roleid)

    def _to_role(self, result):
        role = Role()
        role.id = result[0]
        role.index = result[1]
        role.is_default = result[2]
        role.permissions = result[3]
        return role

    def _to_user(self, result):
        user = User()
        user.userid = result[0]
        user.confirmed = result[1]
        user.username = result[2]
        user.first = result[3]
        user.last = result[4]
        user.email = result[5]
        user.password_hash = result[6]
        user.roleid = result[7]
        return user