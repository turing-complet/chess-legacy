
from chess_app.data_access.repositorybase import RepositoryBase
from chess_app.models import Game
from datetime import datetime


class GameRepository(RepositoryBase):

    def __init__(self, dsn):
        super(GameRepository, self).__init__(dsn)

    def new_game(self, game):
        super().run_sql(
            'add_game',
            id=game.id,
            start=game.start,
            white=game.white,
            black=game.black,
            turn=game.turn
        )

    def find_game(self, gameid):
        result = self.run_sql('find_game', game_id=gameid)
        board = self.run_sql_dict('get_current_board', _game_id=gameid)
        game = self._to_game(result[0])
        game.board = self._to_board(board)
        return game

    def save_board(self, gameid, board):
        # super().run_sql('save_current_board', **dict(board, id=gameid, update_time=datetime.now()))
        super().run_sql('save_current_board',
                        id=gameid,
                        update_time=datetime.now(),
                        wp=board['wP'],
                        wn=board['wN'],
                        wb=board['wB'],
                        wr=board['wR'],
                        wq=board['wQ'],
                        wk=board['wK'],
                        bp=board['bP'],
                        bn=board['bN'],
                        bb=board['bB'],
                        br=board['bR'],
                        bq=board['bQ'],
                        bk=board['bK']
                        )

    def list_games(self, userid):
        rows = super().run_sql('list_games', userid=userid)
        return [self._to_game(row) for row in rows]

    def _to_game(self, result):
        if len(result) == 0:
            return None
        row = result
        game = Game()
        game.id = row[0]
        game.start = row[1]
        game.end = row[2]
        game.white = row[3]
        game.black = row[4]
        game.turn = row[5]
        game.opponent = row[6]  # model semantics are confused
        return game

    def _to_board(self, db_board):
        if len(db_board) == 0:
            return None
        row = db_board[0]
        board = {}
        board['wP'] = row['wp']
        board['wN'] = row['wn']
        board['wB'] = row['wb']
        board['wR'] = row['wr']
        board['wQ'] = row['wq']
        board['wK'] = row['wk']
        board['bP'] = row['bp']
        board['bN'] = row['bn']
        board['bB'] = row['bb']
        board['bR'] = row['br']
        board['bQ'] = row['bq']
        board['bK'] = row['bk']
        return board