
window.onload = function () {

    loadGame(getGameId())
}

function getGameId() {
    return document.getElementById("gameid").innerText
}

function getOrientation() {
    return document.getElementById("orientation").innerHTML;
}

function loadGame(gameid) {
    $.ajax({
        url: "http://localhost/games/" + gameid + '/board',
        success: function(data) {
            position = data.board;
            board = ChessBoard('board', configureBoard(position));
        }
    });
}

function configureBoard(position) {
    if (JSON.stringify(position) === JSON.stringify({})) {
        position = 'start'
    }
    return {
        draggable: true,
        orientation: getOrientation(),
        position: position,
        onDrop: log_stuff
    };
}

function log_stuff(source, target, piece, newPos, oldPos, orientation) {
    console.log("Source: " + source);
    console.log("Target: " + target);
    console.log("Piece: " + piece);
    console.log("New position: " + ChessBoard.objToFen(newPos));
    console.log("Old position: " + ChessBoard.objToFen(oldPos));
    console.log("Orientation: " + orientation);
    console.log("--------------------");
};

function sendBoardState() {
    var gameid = getGameId();
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost/games/" + gameid + "/board",
      "method": "POST",
      "headers": {
        "content-type": "application/x-www-form-urlencoded",
        "cache-control": "no-cache",
      },
      "data": {
        "board": JSON.stringify(board.position())
      }
    }
    $.ajax(settings).done(function (response) {
      console.log(response);
    });
}