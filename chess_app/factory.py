from flask import Flask
from chess_app.config import config, Config, load_environment
import logging
import traceback
import os
from flask_uuid import FlaskUUID


def create_app():
    app = Flask(__name__)
    FlaskUUID(app)
    # logging.basicConfig(format='%(asctime)s|%(module)s|%(levelname)s|%(message)s',
    #                     filename='chess.log',
    #                     level=logging.DEBUG)

    config_path = os.path.abspath(os.path.join(app.instance_path, '../config.env'))
    load_environment(config_path)
    app.config.from_object(config['development'])
    register_blueprints(app)
    register_error_handlers(app)
    return app


def register_blueprints(app):
    from chess_app.controllers import account_blueprint, games_blueprint, home_blueprint

    app.register_blueprint(account_blueprint)
    app.register_blueprint(games_blueprint)
    app.register_blueprint(home_blueprint)


def register_error_handlers(app):
    @app.errorhandler(Exception)
    def handle_exception(e):
        logger = logging.getLogger(__name__)
        stack = traceback.format_exc()
        logger.critical('Exception during request: %s', stack)
        # return stack

    app.register_error_handler(500, lambda e: 'Oops, something went wrong')
