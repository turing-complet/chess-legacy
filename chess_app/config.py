
import os
import logging
logger = logging.getLogger(__name__)


def load_environment(fileName):
    if not os.path.exists(fileName):
        logger.critical('Failed to load environment vars from %s', fileName)
        raise IOError('Configuration not found')
    for line in open(fileName):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


class Config:
    if os.environ.get('SECRET_KEY'):
        SECRET_KEY = os.environ.get('SECRET_KEY')

    COOKIE = 'ca_session'
    DATABASE_URI = 'postgres://postgres:postgres@db:5432/chessdb'
    ADMIN_PASSWORD = os.environ.get('ADMIN_PASSWORD') or 'password'
    ADMIN_EMAIL = os.environ.get(
        'ADMIN_EMAIL') or 'jhagg314@gmail.com'


config = {
    'development': Config,
}
