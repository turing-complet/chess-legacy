from flask import request
from chess_app.config import Config
from jwt import decode


def user_token():
    encoded = request.cookies[Config.COOKIE]
    decoded = decode(encoded, Config.SECRET_KEY, verify=False)
    return decoded


def current_username():
    token = user_token()
    return token['username']


def current_userid():
    token = user_token()
    return token['userid']