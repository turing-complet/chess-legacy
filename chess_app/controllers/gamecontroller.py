from flask import render_template, request, make_response, jsonify, Blueprint
from chess_app import request_state
from chess_app.auth import authenticate
from chess_app.models import Game
from chess_app.services.game_service import GameService
from chess_app.services.user_service import UserService
import logging
import json

games_blueprint = Blueprint('games', __name__, template_folder='templates')
log = logging.getLogger(__name__)


@games_blueprint.route('/games/invite', methods=['GET'])
def invite():
    return render_template('invite.html')


@games_blueprint.route('/games', methods=['GET'])
@authenticate
def list_games(game_service=GameService()):
    userid = request_state.current_userid()
    games = game_service.list_games(userid)
    args = [(game.id, game.start, game.color(userid), game.opponent) for game in games]
    return make_response(render_template('game_list.html', games=list(args)), 200)

    
@games_blueprint.route('/games/<uuid:gameid>')
@authenticate
def render_game(gameid):
    orientation = request.args.get('color')
    return render_template('game.html', gameid=gameid, orientation=orientation)


@games_blueprint.route('/games/<uuid:gameid>/board')
def load_game(gameid, game_service=GameService()):
    board = game_service.find_game(gameid).board
    return make_response(jsonify(board=board), 200)


@games_blueprint.route('/games/<uuid:gameid>/board', methods=['POST'])
def save_board(gameid, game_service=GameService()):
    board = json.loads(request.form['board'])
    # log.debug(request.form['board'])
    game_service.save_board(gameid, board)
    return make_response(jsonify(message="move was saved"), 200)


@games_blueprint.route('/games/new/', methods=['POST'])
@authenticate
def create(game_service=GameService(), user_service=UserService()):
    opponent = request.form['username']
    color = request.form['color']
    userid1 = request_state.current_userid()
    userid2 = user_service.get_by_username(opponent).userid
    game = Game.build_from_invite(userid1, userid2, color)
    game_service.new_game(game)
    return make_response("created a game", 200)


