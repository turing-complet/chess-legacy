from flask import render_template, request, jsonify, make_response, \
    redirect, url_for, Blueprint

from chess_app import auth
from chess_app.services.user_service import UserService
from chess_app.models.user import User
from chess_app.config import Config
import logging

account_blueprint = Blueprint('accounts', __name__)
logger = logging.getLogger(__name__)


@account_blueprint.route('/accounts', methods=['GET'])
def register():
    return render_template('create_account.html')


@account_blueprint.route('/accounts', methods=['POST'])
def create_account(user_service=UserService()):
    username = request.form['username']
    password = request.form['password']
    email = request.form['email']
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    user = user_service.get_by_username(username)
    if user is None:
        roleid = user_service.get_default_role().id
        user = User.create(username=username, password=password, email=email,
                           first=first_name, last=last_name, roleid=roleid)
        user_service.add_user(user)
    else:
        return make_response("Account exists", 409)
    return make_response("Account created", 201)


@account_blueprint.route('/login', methods=['GET'])
def login():
    return render_template('login.html')


@account_blueprint.route('/login', methods=['POST'])
def login_submit(user_service=UserService()):
    username = request.form['username']
    password = request.form['password']
    user = user_service.get_by_username(username)
    if user is None:
        return make_response(jsonify(message="User not found", username=username), 404)
    if not user.verify_password(password):
        return make_response("Unauthorized", 401)
    else:
        token = auth.generate_token(user)
        logger.debug('logging in user=%s', username)
        # response = make_response(jsonify(message="success", token=str(token, 'utf-8')), 200)
        response = make_response(render_template('index.html'))
        response.set_cookie(Config.COOKIE, token)
        return response


@account_blueprint.route('/logout')
def logout():
    response = make_response(redirect(url_for('home.page_index')))
    response.set_cookie(Config.COOKIE, '', expires=0)
    return response
