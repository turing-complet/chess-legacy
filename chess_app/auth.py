from functools import wraps
from flask import request, redirect, url_for
from chess_app.config import Config
from datetime import datetime, timedelta
from jwt import decode, encode, DecodeError
import logging

logger = logging.getLogger(__name__)


def generate_token(user):
    logger.debug('Issuing token for user %s', user.username)
    return encode({
        'username': user.username,
        'userid': user.userid,
        # 'role': user.role.name,
        'expires': str(datetime.now() + timedelta(hours=3))},
        Config.SECRET_KEY, algorithm='HS256')


def validate(token):
    try:
        decoded = decode(token, Config.SECRET_KEY, verify=True)
        expiration = datetime.strptime(decoded['expires'], "%Y-%m-%d %H:%M:%S.%f")
        if expiration < datetime.now():
            return False
        return True
    except KeyError:
        raise Exception("You got yourself a malformed token, there.")
    except DecodeError:
        logger.critical('Failed to decode token: %s', str(token))
        return False


def authenticate(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        token = request.cookies.get(Config.COOKIE)
        if not token or not validate(token):
            # resp = make_response()
            # resp.headers["WWW-Authenticate"] = "Basic realm=Login Required"
            # return resp
            return redirect(url_for('accounts.login'))
        return func(*args, **kwargs)
    return decorated
