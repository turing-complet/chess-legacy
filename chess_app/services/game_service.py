from chess_app.data_access import GameRepository
from chess_app.config import Config


class GameService:

    def __init__(self):
        self.game_repo = GameRepository(Config.DATABASE_URI)

    def list_games(self, userid):
        return self.game_repo.list_games(userid)

    def find_game(self, gameid):
        game = self.game_repo.find_game(gameid)
        game.board = self.position_to_piece(game.board)
        return game

    def save_board(self, gameid, board):
        self.game_repo.save_board(gameid, self.piece_to_positions(board))

    def new_game(self, game):
        self.game_repo.new_game(game)

    # chessboard.js uses position -> piece mapping,
    # need to switch to store fixed columns
    # TODO test this
    def piece_to_positions(self, board):
        db_board = {}
        pairs = board.items()
        pieces = set([x[1] for x in pairs])
        for k in pieces:
            db_board[k] = []

        for c in pairs:
            db_board[c[1]].append(c[0])

        return db_board

    # takes {piece -> list<position>} and returns {position -> piece}
    # TODO test this
    def position_to_piece(self, board):
        ui_board = {}
        if board is None:
            return ui_board
        for piece in board.keys():
            for position in board[piece]:
                ui_board[position] = piece
        return ui_board