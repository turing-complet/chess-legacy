
from chess_app.data_access.user_repository import UserRepository
from chess_app.config import Config
from chess_app.models.user import User


class UserService:

    def __init__(self):
        self._repo = UserRepository(Config.DATABASE_URI)

    def get_by_username(self, username):
        return self._repo.get_by_username(username)

    def get_default_role(self):
        return self._repo.get_default_role()

    def add_user(self, user):
        return self._repo.add_user(user)
