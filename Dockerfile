FROM ubuntu:16.04

RUN apt-get update -y
RUN apt-get install -y python-virtualenv python-pip
RUN pip install virtualenv

RUN virtualenv -p $(which python3) /appenv
RUN /appenv/bin/pip install --upgrade pip

COPY requirements.txt /chess/
RUN /appenv/bin/pip install -r /chess/requirements.txt 

ENV FLASK_APP chess_app/chess.py
ENV LANG C.UTF-8
EXPOSE 3000

COPY . /chess
WORKDIR /chess

ENTRYPOINT /appenv/bin/flask run 
